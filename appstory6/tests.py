from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import*
from .forms import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest

"""

Unit Tests

"""

class Lab6_Test(TestCase):
	def test_lab_6_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200) # If client access this, it would give 200 as a response.

	def test_lab6_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, landingPage, delete_new)


	def test_model_can_create_new_status(self):
		# Creating a new activity
		new_status = Status.objects.create(statusku="hehe")

		# Retrieving all available activity
		counting_all_available_todo = Status.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)

	def test_form_todo_input_has_placeholder_and_css_classes(self):
		form = formStatus()
		self.assertIn('class="todo-form-input', form.as_p())
		# form.as_p - as paragraph?
		# To Check if This Class is Already on The Website or Not

	def test_form_validation_for_blank_items(self):
		form = formStatus(data={'statusku': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
		form.errors['statusku'], ["This field is required."]
		)

	def test_lab6_post_success_and_render_the_result(self):
		test = 'hehe'
		response_post = Client().post('/addStatus', {'statusku': test})
		self.assertEqual(response_post.status_code, 302) # Go to The Database, if it isnt there, it would give 302 as a response

		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)
		self.assertIn("Hey, What's Up?", html_response)

	def test_lab6_profile_page_is_exist(self):
		response = Client().get('/about-me')
		self.assertEqual(response.status_code, 200)

	def test_lab6_profile_has_name(self):
		response = Client().get('/about-me')
		html_response = response.content.decode('utf8')
		self.assertIn("Shafira Dyah Pradita", html_response)

	def test_lab6_profile_has_description(self):
		response = Client().get('/about-me')
		html_response = response.content.decode('utf8')
		self.assertIn('class="description"', html_response)

	def test_lab6_using_about_func(self):
		found = resolve('/about-me')
		self.assertEqual(found.func, about)

	def test_lab6_delete_function_works(self):
		response = Client().get('/delete_new')
		self.assertEqual(response.status_code, 302)



"""

Functional tests


"""

class Lab7_FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless') 
		chrome_options.add_argument('disable-gpu')
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		self.selenium.implicitly_wait(25) 
		super(Lab7_FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Lab7_FunctionalTest, self).tearDown()

	def test_input_status(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get('http://127.0.0.1:8000/')
		# find the form element
		statusnya = selenium.find_element_by_id('id_statusku')

		submit = selenium.find_element_by_id('submit')

		# Fill the form with data
		statusnya.send_keys('Coba Coba')
		#time.sleep(5)

		# submitting the form
		submit.send_keys(Keys.RETURN)
		#time.sleep(5)
		self.assertIn("Coba Coba", selenium.page_source)

	def test_layout_is_correct(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get('http://shadybelajarppw.herokuapp.com/about-me')
		# find the form element
		name = selenium.find_element_by_tag_name('h1')

		# Fill the form with data

		# submitting the form
		time.sleep(5)
		self.assertIn("Shafira Dyah Pradita", name.text)

	def test_css_is_correct(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get('http://shadybelajarppw.herokuapp.com/about-me')
		# find the form element
		selector = selenium.find_element_by_css_selector('div.roundedrec')
		selector_2 = selenium.find_element_by_css_selector('img.img-2')
		selector_3 = selenium.find_element_by_tag_name('body')

		css_property = selector_3.value_of_css_property('background-image')
		css_property_2 = selector.value_of_css_property('border-radius')

		# submitting the form
		time.sleep(5)
		self.assertIn('class="roundedrec"', selenium.page_source)
		self.assertEqual('linear-gradient(90deg, rgb(242, 122, 84) -21.05%, rgb(168, 87, 229) 109.28%, rgb(161, 84, 242) 121.05%)', css_property)
		self.assertEqual('10px', css_property_2)

