from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import *
from .forms import *
# Create your views here.

response={}

def about(request):
	return render (request, 'about.html', {})


def landingPage(request):
	kabar = Status.objects.all()
	response['kabar'] = kabar
	response['formStatus'] = formStatus
	return render(request, 'landing_page.html', response)

def addStatus(request):
	form = formStatus(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['statusku'] = request.POST['statusku']
		kabar = Status(statusku=response['statusku'])
		kabar.save()
		return HttpResponseRedirect('/')

def delete_new(request):
    kabar = Status.objects.all().delete()
    return redirect('/')
