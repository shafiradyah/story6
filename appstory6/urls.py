from django.urls import path
from django.contrib import admin
from . import views
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import *


urlpatterns = [
   path('', landingPage, name="landingpage"),
   path('about-me', about, name='about-me'),
   path('addStatus', addStatus, name="addStatus"),
   path('delete_new', delete_new, name='delete_new'),
    

]

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()
