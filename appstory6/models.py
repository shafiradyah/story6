from django.db import models
from django.utils import timezone
# Create your models here.

class Status(models.Model):
    statusku = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)