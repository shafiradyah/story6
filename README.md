# Repository Story 6 - 9 | PPW

```bash
Name        : Shafira Dyah Pradita
NPM         : 1706984726
Class       : PPW-F
```
## Link Website Herokuapp
[https://shadybelajarppw.herokuapp.com/](https://shadybelajarppw.herokuapp.com/)

## Status Website 
[![pipeline status](https://gitlab.com/shafiradyah/story6/badges/master/pipeline.svg)](https://gitlab.com/shafiradyah/story6/commits/master)
[![coverage report](https://gitlab.com/shafiradyah/story6/badges/master/coverage.svg)](https://gitlab.com/shafiradyah/story6/commits/master)