from django.urls import path
from django.contrib import admin
from cerita import views
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import *


urlpatterns = [
    path('cerita', views.cerita, name='cerita'),
    path('<str:judul>', cerita_view)
]

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()
