from django.db import models

# Create your models here.
class Cerita(models.Model):
	judul = models.CharField(max_length=100, primary_key=True)
	gambar = models.CharField(max_length=200)
	snippet = models.CharField(max_length=1000)
	isi = models.CharField(max_length=5000, default='testinggggggg')
	created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
