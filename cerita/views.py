from django.shortcuts import render
from django.shortcuts import get_object_or_404
from .models import Cerita

# Create your views here.
response = {}
def cerita(request):
	stories = Cerita.objects.all()
	return render(request, 'cerita.html', {'stories': stories})

def cerita_view(request, judul):
    story = get_object_or_404(Cerita, pk=judul)
    return render(request, 'ceritaku.html', {'judul': story.judul,
                                                 'snippet':story.snippet, 'isi':story.isi,
                                                 'foto': story.gambar})
