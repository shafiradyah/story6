from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import subscribe, validate
from .models import Subscriber
from .forms import SubscribeForm
import unittest

# Create your tests here.
class Lab10_Test(TestCase):
    def test_lab10_url_is_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_lab10_using_subscribe_func(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, subscribe)

    def test_lab10_using_validate_func(self):
        found = resolve('/validate/')
        self.assertEqual(found.func, validate)

    def test_model_can_create_new_subscriber(self):
        new_subscriber = Subscriber.objects.create(name="me", email="itsme@yahoo.com", password="hello1234")
        counting_all_subscriber = Subscriber.objects.all().count()
        self.assertEqual(counting_all_subscriber, 1)
    
    def test_max_length_name(self):
        name = Subscriber.objects.create(name="gini loh gini loh gini lohgini")
        self.assertLessEqual(len(str(name)), 50)


