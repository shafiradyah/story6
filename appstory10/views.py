from django.shortcuts import render
from .forms import SubscribeForm
from django.http import HttpResponseRedirect, HttpResponse
from .models import Subscriber
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from django.core import serializers

# Create your views here.
def subscribe(request):
    response = {}
    users = Subscriber.objects.all()
    response["forms"] = SubscribeForm()
    response["users"] = users
    return render(request, 'subscribe.html', response)


@csrf_exempt
def validate(request):
    email = request.POST.get("email")
    data = {'not_valid': Subscriber.objects.filter(email__iexact=email).exists()
    }
    return JsonResponse(data)

def success(request):
    submited_form = SubscribeForm(request.POST or None)
    if (submited_form.is_valid()):
        cd = submited_form.cleaned_data
        new_subscriber = Subscriber(name=cd['name'], password=cd['password'], email=cd['email'])
        new_subscriber.save()
        data = {'name': cd['name'], 'password': cd['password'], 'email': cd['email']}
    elif (request.method == "GET"):
        obj = Subscriber.objects.all()
        data = serializers.serialize('json', obj)
    return JsonResponse(data, safe=False)

@csrf_exempt
def delete_new(request):
    if (request.method == "POST"):
        subId = request.POST['pk']
        Subscriber.objects.filter(pk=subId).delete()
        return HttpResponse(json.dumps({'message': "Succeed"}),content_type="application/json")
    else:
        return HttpResponse(json.dumps({'message': "There's something wrong. Try again."}),content_type="application/json")





