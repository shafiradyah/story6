from django.urls import path
from .views import *

urlpatterns = [
    path('subscribe/', subscribe, name="subscribe"),
    path('validate/', validate),
    path('success/', success, name="success"),
    path('delete-new/', delete_new, name="delete-new")

]