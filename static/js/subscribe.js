$(document).ready(function() {
    var name = "";
    var password = "";
    $("#id_name").change(function() {
        name = $(this).val();
    });
    $("#id_password").change(function() {
        password = $(this).val();
    })
    $("#id_email").change(function() {
        var email = $(this).val();
        console.log(email)
        $.ajax({
            url: "/validate/",
            data: {
                'email': email,
            },
            method: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data)
                if(data.not_valid) {
                    alert("This email is already taken, please use another one.");
                } else {
                    if(name.length > 0 && password.length > 5) {
                        $("#submit_subscribe").removeAttr("disabled");
                    }
                }
                
            }
        });
    });
    $("#submit_subscribe").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            url:"/success/",
            data: $("form").serialize(),
            method: 'POST',
            dataType: 'json',
            success: function(data){
                swal({
                    title: "Hello " + data.name,
                    text: 'Thanks for subscribing.',
                    type: 'success',
                    showConfirmButton: false,
                    timer: 2500
                    }).then(function() {
                        window.location = "/subscribe/"
                    });
            }, error: function() {
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong! Redirecting you back to profile page...',
                    showConfirmButton: false,
                    timer: 2500
                  }).then(function() {
                      window.location = "/about-me"
                  });
            }
        })
    });
     $.ajax({
            url: "/success/",
            success: function(data){
                var header = "<thead><tr><th>Name</th><th>Email</th></tr></thead>";
                $("#subscriber").append(header);
                $("#subscriber").append('<tbody>');
                var result = JSON.parse(data)
                console.log(result[1])
                for (i=0; i<result.length; i++) {
                var tmp = "<tr><td>" + result[i].fields.name + "</td><td>" + result[i].fields.email
                + "</td><td>" + "<a href=" + "'javascript:void(0)' onclick='deleteSub(" + result[i].pk + ")' style='padding-right:25px;'>Unsubscribe</a> </tr>";
                $('#subscriber').append(tmp);
            }
                $('#subscriber').append('<tbody');

             
        }




});
});

function deleteSub(pk){
        console.log(pk)
        $.ajax({
            method:"POST",
            url: "/delete-new/",
            data:{'pk':pk},
            success:function(){
                swal({
                    title: "Goodbye!",
                    text: 'Thanks for subscribing.',
                    type: 'success',
                    showConfirmButton: false,
                    timer: 2500
                    }).then(function() {
                        window.location = "/subscribe/"
                    }); 

            }
        });
    }