from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.core import serializers
import requests
import json
# Create your views here.



response = {}

def books(request):
	if request.user.is_authenticated:
		request.session['user'] = request.user.username
		request.session['email'] = request.user.email
		request.session['first_name'] = request.user.first_name
		request.session['last_name'] = request.user.last_name
		
		#request.session.get('counter', 1)
		request.session.setdefault('counter',0)
		print(dict(request.session))
		for key, value in request.session.items():
			print('{} => {}'.format(key, value))
	return render(request, 'home.html') 

def datas(request):
	search = request.GET.get('cari')
	URL = "https://www.googleapis.com/books/v1/volumes?q="+search
	dts = requests.get(URL).json()
	return JsonResponse(dts)

def tambah(request):
	print(dict(request.session))
	request.session['counter'] = request.session['counter'] + 1
	return HttpResponse(request.session['counter'], content_type = "application/json")

def kurang(request):

	request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type = "application/json")


