from django.urls import path
from django.contrib import admin
from . import views 
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import *


urlpatterns = [
    # path('delete', views.delete, name="delete"),
    path('books', views.books, name='books'),
    url(r'data', datas, name = 'datas'),
    # path('getdata', views.getData, name='getdata')
    path('tambah/', views.tambah, name="tambah"),
	path('kurang/', views.kurang, name="kurang"),


]

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()
