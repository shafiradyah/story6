from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
# Create your tests here.


class Lab9UnitTest(TestCase):
    def test_lab9_uses_story9bookshelf_as_template(self):
        response = Client().get('/books')
        self.assertTemplateUsed(response, 'home.html')
    def test_lab9_url_exists(self):
        response = Client().get('/books')
        self.assertEqual(response.status_code, 200)
    def test_json_data_1_url_exists(self):
        response = Client().get('/data?cari=comic')
        self.assertEqual(response.status_code, 200)
    def test_json_data_2_url_exists(self):
        response = Client().get('/data?cari=quilting')
        self.assertEqual(response.status_code, 200)
    def test_json_data_3_url_exists(self):
        response = Client().get('/data?cari=architecture')
        self.assertEqual(response.status_code, 200)
